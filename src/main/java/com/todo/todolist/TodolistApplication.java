package com.todo.todolist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.todo.todolist.model.TodoItem;
import com.todo.todolist.repository.TodoRepository;
import com.todo.todolist.service.TodoService;

@SpringBootApplication
public class TodolistApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(TodolistApplication.class, args);
	}

	@Autowired
	private TodoRepository todoRepository;

	@Autowired
	private TodoService todoService;

	@Override
	public void run(String... args) throws Exception {
		TodoItem todoItem = new TodoItem();
		todoItem.setTitle("TitreN1");
		todoItem.setDescription("Description1");
		todoItem.setDone(false);
		todoRepository.save(todoItem);
		System.out.println("Création d'une todo hard-coded");
		System.out.println(todoItem);
	}
}
