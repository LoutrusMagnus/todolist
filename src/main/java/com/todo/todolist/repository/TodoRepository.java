package com.todo.todolist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todo.todolist.model.TodoItem;

public interface TodoRepository extends JpaRepository<TodoItem, Long> {

}
