package com.todo.todolist.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class TodoItem {
	
	@Id
	@GeneratedValue
	private Long id;
	@NotBlank
	private String title;
	private String description;
	private boolean done;

	
}

