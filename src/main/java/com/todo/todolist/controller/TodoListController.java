package com.todo.todolist.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todo.todolist.model.TodoItem;
import com.todo.todolist.service.TodoService;

@RestController
@RequestMapping(value = "/todo")
public class TodoListController {
	
	@Autowired
	private TodoService todoService;
	
	@GetMapping
	public List<TodoItem> findAll(){
		return todoService.findAll();
	}
	
	@PostMapping
	public TodoItem save(@Valid @NotNull @RequestBody TodoItem todoItem) {
		return todoService.save(todoItem);
	}
	
	@PutMapping
	public TodoItem update(@Valid @NotNull @RequestBody TodoItem todoItem) {
		return todoService.update(todoItem);
	}
	
	@DeleteMapping(value= "/{id}")
	public void delete(@PathVariable Long id) {
		todoService.deleteById(id);
	}
}
