package com.todo.todolist.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.todo.todolist.model.TodoItem;
import com.todo.todolist.repository.TodoRepository;

@Service
public class TodoService {
	
	@Autowired
	private TodoRepository todoRepository;

	public List<TodoItem> findAll() {
		return todoRepository.findAll();
	}

	public TodoItem save(TodoItem todoItem) {
		return todoRepository.save(todoItem);
	}

	public TodoItem update(TodoItem todoItem) {
		return todoRepository.save(todoItem);
	}

	public void deleteById(Long id) {
		todoRepository.deleteById(id);
	}

	public Optional<TodoItem> findById(Long id) {
		return todoRepository.findById(id);
	}
}
