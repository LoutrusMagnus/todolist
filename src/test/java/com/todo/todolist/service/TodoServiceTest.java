package com.todo.todolist.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.todo.todolist.model.TodoItem;
import com.todo.todolist.repository.TodoRepository;

public class TodoServiceTest {
	AutoCloseable closeable;
	@Mock
	private TodoRepository repository;
	@InjectMocks
	private TodoService service;

	@BeforeEach
	public void Initialize() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@AfterEach
	public void close() throws Exception {
		closeable.close();
	}

	@Test
	public void save_Ok() {
		TodoItem itemCreated = new TodoItem(null, "testTitre", "testDescription", false);
		when(this.repository.save(any(TodoItem.class))).thenAnswer((d) -> {
			TodoItem todoItem = d.getArgument(0);
			todoItem.setId(2L);
			return todoItem;
		});
		TodoItem createdItem = this.service.save(itemCreated);
		assertNotNull(createdItem.getTitle());
		assertNotNull(createdItem.getId());
		assertEquals("testTitre", createdItem.getTitle());
		assertEquals("testDescription", createdItem.getDescription());
	}
}

